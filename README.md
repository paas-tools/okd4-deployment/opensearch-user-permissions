# OpenSearch User Permissions Synchronization

This component synchronizes (and translates) permissions from an OKD project to an OpenSearch cluster.

The workflow is:

* Users opens `logs.$CLUSTER.cern.ch` and authenticates through OpenShift OAuth provider
* The Oauth proxy passes the access token of the user onto the application
* The application retrieves the list of projects the user has access to with the access token
* Based on this list, a suitable DLS ([Document-Level Security](https://opensearch.org/docs/latest/security-plugin/access-control/document-level-security/)) query is constructed -- **this ensures the user can only view logs for projects he/she manages**
* Using a privileged OpenSearch service account (see below),  the application creates appropriates [Roles and RoleMappings in OpenSearch](https://opensearch.org/docs/latest/security-plugin/access-control/users-roles/)
* After this is completed, the user is redirected to the Web UI of the OpenSearch cluster

See `main.go` for implementation details.

## OpenSearch service account

To interact with the API, an ["internal" user](https://opensearch.org/docs/latest/security-plugin/access-control/users-roles/#create-users) needs to be created in the OpenSearch instance and it needs to be [mapped to the `security_rest_api_access` role](https://opensearch.org/docs/latest/security-plugin/access-control/api/#access-control-for-the-api).

## Configuration

The component is configured via environment variables.
In the Helm chart, these can be set by supplying a suitable Kubernetes Secret.
The following variables are supported:

* `CLUSTER_NAME`
* `OPENSEARCH_API_URL` (usually `https://os-openshift.cern.ch/os`)
* `OPENSEARCH_TENANT_NAME` (e.g. `okd_paas_users`)
* `OPENSEARCH_INDEX_PATTERN` (e.g. `monit_private_openshift_logs_paas-*`)
* `OPENSEARCH_API_USERNAME` (name of service account from above)
* `OPENSEARCH_API_PASSWORD` (password of service account from above)
* `DEBUG_LOG` (disabled by default, set to `true` to enable)

Note that the root certificate of the [CERN Grid CA](https://ca.cern.ch/cafiles/certificates/Grid.aspx) needs to be installed because the OpenSearch instances use certificates signed by this CA. Otherwise you will be greeted with the following error message:

```
curl: (60) SSL certificate problem: certificate has expired
More details here: https://curl.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```

## References

* [OpenSearch API documentation](https://opensearch.org/docs/latest/security-plugin/access-control/api/)
* [OpenSearch Access Control Overview](https://opensearch.org/docs/latest/security-plugin/access-control/index/)
* [Boolean Queries for Document-Level Security](https://opensearch.org/docs/latest/opensearch/query-dsl/bool/)
