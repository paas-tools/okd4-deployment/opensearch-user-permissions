#!/bin/bash

set -euo pipefail

# Prints a timestamp in the usual Kubernetes format
_timestamp() {
  date "+%Y-%m-%dT%H:%M:%S.%3NZ"
}

# Performs an API call to the OpenSearch server
# arguments: <METHOD> <URI> <CURL_ARGS>
# example: `os_api GET /_plugins/_security/api/roles/ --data foo=bar`
_os_api() {
    curl -sS "${OS_ENDPOINT}${2}" \
         --request "$1" \
         --header 'Content-type: application/json' \
         --basic --user "${OS_API_USERNAME}:${OS_API_PASSWORD}" \
         "${@:3}" # pass all resulting arguments (minus the ones we already used) onto curl
}

# This function deletes OpenSearch roles (and associated mappings) for which no matching namespace exists in the cluster
_cleanup_old_roles() {
    # Get all roles starting with the prefix
    HAVE_ROLES=$(_os_api GET "/_plugins/_security/api/roles/" | \
                     jq -r --arg role "${ROLE_PREFIX}" 'to_entries | .[] | select(.key | startswith($role)) | .key' | sort)

    # Get the current list of namespaces
    NAMESPACES=$(oc get namespaces -l "${NAMESPACE_LABEL_SELECTOR}" -o jsonpath='{.items[*].metadata.name}' | sed 's/ /\n/g' | sort)
    EXPECTED_ROLES=$(echo "$NAMESPACES" | sed "s/^/${ROLE_PREFIX}/")

    # Compare the list of roles to the list of namespaces
    # --fixed-strings (-F) makes sure that special characters do not get treated as a regex
    # --line-regexp (-x) makes sure we're only considering exact matches (prepends ^ and appends $)
    # --invert-match (-v) print the the results that are NOT present in $EXPECTED_ROLES
    # --file (-f) reads the list of patterns from the given file
    EXISTS_IN_OS_BUT_NOT_IN_CLUSTER=$(grep --fixed-strings --line-regexp --invert-match --file <(echo "$EXPECTED_ROLES") <<< "$HAVE_ROLES" || true)

    NUM_EXTRA_ROLES=$(echo "$EXISTS_IN_OS_BUT_NOT_IN_CLUSTER"| wc -w)
    echo "Found ${NUM_EXTRA_ROLES} roles that exist in OpenSearch but don't have a matching namespace in the cluster."

    # Perform cleanup
    # https://opensearch.org/docs/latest/security/access-control/api/#delete-role
    if [ $NUM_EXTRA_ROLES -gt 0 ]; then
        for role in $EXISTS_IN_OS_BUT_NOT_IN_CLUSTER; do
            if ! _dry_run; then
                # deleting a role also deletes the associated mappings
                _os_api DELETE "/_plugins/_security/api/roles/${role}"
            else
                echo "- ${role}"
            fi
        done

    fi
}

# Creates or updates an OpenSearch role for the given namespace
# arguments: <NAMESPACE>
_update_role_for_namespace() {
    NAMESPACE="$1"
    ROLE_NAME="${ROLE_PREFIX}${NAMESPACE}"
    # Lookup the project admins - they will be allowed to access (read) the logs
    PROJECT_OWNER="$(oc get namespace "$NAMESPACE" -o jsonpath='{.metadata.labels.lifecycle\.webservices\.cern\.ch/owner}')"
    PROJECT_ADMIN="$(oc get namespace "$NAMESPACE" -o jsonpath='{.metadata.labels.lifecycle\.webservices\.cern\.ch/adminGroup}')"

    # Create (or replace) a role that allows access *only* to document from this cluster and namespace combination
    # https://opensearch.org/docs/latest/security-plugin/access-control/api/#create-role
    # https://opensearch.org/docs/latest/opensearch/query-dsl/bool/
    DLS_QUERY=$(jq --null-input --compact-output --arg CLUSTER_NAME "$CLUSTER_NAME" --arg NAMESPACE "$NAMESPACE" '
{
  "bool": {
    "must": [
      {
        "match": {
          "data.cluster_name": $CLUSTER_NAME
        }
      },
      {
        "match": {
          "data.namespace": $NAMESPACE
        }
      }
    ]
  }
}
')

    # `read` grants read permissions such as search, get field mappings, get, and mget.
    # `kibana_all_read` is required to use the web UI
    # https://opensearch.org/docs/latest/security/access-control/default-action-groups/
    echo "Creating role '${ROLE_NAME}' on index '${INDEX_PATTERN}' ..."
    if ! _dry_run; then
        # Use jq to ensure proper escaping of all fields
        ROLE=$(jq --null-input --arg INDEX_PATTERN "$INDEX_PATTERN" --arg DLS "$DLS_QUERY" '{
  "cluster_permissions": [],
  "index_permissions": [
    {
      "index_patterns": [
        $INDEX_PATTERN
      ],
      "dls": $DLS,
      "allowed_actions": ["read"],
      "fls": [],
      "masked_fields": []
    }
  ],
  "tenant_permissions": [
    {
      "tenant_patterns": ["global_tenant"],
      "allowed_actions": ["kibana_all_read"]
    }
  ]
}
')

        _os_api PUT "/_plugins/_security/api/roles/${ROLE_NAME}" --data @- <<< "$ROLE"
        echo "" # print a newline after response
    fi

    # Assign users/groups to the role
    # https://opensearch.org/docs/latest/security-plugin/access-control/api/#create-role-mapping
    echo "Assigning user '${PROJECT_OWNER}' and group '${PROJECT_ADMIN}' to role '${ROLE_NAME}' ..."
    if ! _dry_run; then
        ROLEMAPPING=$(jq --null-input --arg PROJECT_OWNER "$PROJECT_OWNER" --arg PROJECT_ADMIN "$PROJECT_ADMIN" '
{
  "users": [
    $PROJECT_OWNER
  ],
  "backend_roles": [
    $PROJECT_ADMIN
  ],
  "hosts": []
}
')
        _os_api PUT "/_plugins/_security/api/rolesmapping/${ROLE_NAME}" --data @- <<< "$ROLEMAPPING"
        echo "" # print newline after response
    fi

    ANNOTATION="${NAMESPACE_UPDATE_ANNOTATION}=$(_timestamp)"
    echo "Recording update in namespace annotation '${ANNOTATION}' ..."
    if ! _dry_run; then
        oc annotate namespace "$NAMESPACE" "$ANNOTATION" --overwrite=true
    fi
}


# Indicates if operations should be run in dry-run mode
DRY_RUN="${DRY_RUN:-false}"
_dry_run() {
    if [[ "$DRY_RUN" == "true" || "$DRY_RUN" == "TRUE" ]]; then
        return 0 # true
    fi
    return 1 # false
}

_print_help() {
    cat - <<EOF
Usage:
  ./log-permissions.sh [update|cleanup] <CLUSTER_NAME> <NAMESPACE_LABEL_SELECTOR>

The following environment variables must be set:
  OS_API_USERNAME
  OS_API_PASSWORD
  OS_ENDPOINT
  INDEX_PATTERN

Optional environment variables:
  DRY_RUN
EOF
}

# Main handler
ACTION="${1:-}"
CLUSTER_NAME="${2:-}"
NAMESPACE_LABEL_SELECTOR="${3:-}"
if [[ -z "$CLUSTER_NAME" || -z "$NAMESPACE_LABEL_SELECTOR" || -z "$OS_API_USERNAME" || -z "$OS_API_PASSWORD" || -z "$OS_ENDPOINT" || -z "$INDEX_PATTERN" ]]; then
    _print_help
    exit 1
fi

# This prefix is used to generate the names of roles in OpenSearch and identify roles belonging to a particular OKD cluster.
# DO NOT CHANGE unless you are aware of the impact.
ROLE_PREFIX="cern-okd4_${CLUSTER_NAME}_"

# The value of this namespace annotation indicates when the last successful update of the OpenSearch role for this namespace occured
NAMESPACE_UPDATE_ANNOTATION="logging.okd.cern.ch/log-permissions-last-update"

if ! oc cluster-info | grep "${CLUSTER_NAME}" > /dev/null; then
    echo "Error: provided connection details (KUBECONFIG) does not match ${CLUSTER_NAME}"
    exit 1
fi

if _dry_run; then
    echo "Info: running all actions in dry-run mode, no changes will be made."
fi

case "$ACTION" in
    "update")
        for ns in $(oc get namespaces -l "$NAMESPACE_LABEL_SELECTOR" -o jsonpath='{.items[*].metadata.name}'); do
            _update_role_for_namespace "$ns"
        done
        ;;
    "cleanup")
        _cleanup_old_roles
        ;;
    *)
        echo "Error: unknown action '${ACTION}'"
        _print_help
        exit 1
esac

echo "Done."
