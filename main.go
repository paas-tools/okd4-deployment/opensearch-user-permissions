package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

var clusterName = mustGetEnv("CLUSTER_NAME")
var clusterApiUrl = "https://" + mustGetEnv("KUBERNETES_SERVICE_HOST") + ":" + mustGetEnv("KUBERNETES_SERVICE_PORT")
var httpClient *http.Client
var projectLabelSelector = "okd.cern.ch/user-project=true"
var httpListenAddress = mustGetEnv("HTTP_LISTEN_ADDRESS")
var opensearchApiUrl = mustGetEnv("OPENSEARCH_API_URL")
var opensearchApiUsername = mustGetEnv("OPENSEARCH_API_USERNAME")
var opensearchApiPassword = mustGetEnv("OPENSEARCH_API_PASSWORD")
var opensearchTenantName = mustGetEnv("OPENSEARCH_TENANT_NAME")
var opensearchIndexPattern = mustGetEnv("OPENSEARCH_INDEX_PATTERN")
var debugLog = strings.ToLower(os.Getenv("DEBUG_LOG"))

func mustGetEnv(name string) string {
	val := os.Getenv(name)
	if val == "" {
		panic("Environment variable '" + name + "' not set")
	}
	return val
}

func debugLogf(format string, v ...any) {
	if debugLog == "true" {
		log.Printf(format, v...)
	}
}

// https://docs.openshift.com/container-platform/4.13/rest_api/project_apis/project-project-openshift-io-v1.html
type ProjectResponse struct {
	Kind       string `json:"kind"`
	APIVersion string `json:"apiVersion"`
	Items      []struct {
		Metadata struct {
			Name              string            `json:"name"`
			UID               string            `json:"uid"`
			ResourceVersion   string            `json:"resourceVersion"`
			CreationTimestamp time.Time         `json:"creationTimestamp"`
			Labels            map[string]string `json:"labels"`
			Annotations       map[string]string `json:"annotations"`
		} `json:"metadata"`
		// NOTE: Spec and Status are omitted
	} `json:"items"`
}

// https://opensearch.org/docs/latest/security/access-control/api/#create-role
type OpensearchRole struct {
	Description        string              `json:"description"`
	ClusterPermissions []string            `json:"cluster_permissions"`
	IndexPermissions   []IndexPermissions  `json:"index_permissions"`
	TenantPermissions  []TenantPermissions `json:"tenant_permissions"`
}
type OpensearchRoleDescription struct {
	CreatedBy   string
	LastUpdated string // RFC3339
}
type IndexPermissions struct {
	IndexPatterns  []string `json:"index_patterns"`
	DLS            string   `json:"dls"`
	AllowedActions []string `json:"allowed_actions"`
	FLS            []string `json:"fls"`
	MaskedFields   []string `json:"masked_fields"`
}
type TenantPermissions struct {
	TenantPatterns []string `json:"tenant_patterns"`
	AllowedActions []string `json:"allowed_actions"`
}

// https://opensearch.org/docs/latest/security/access-control/api/#role-mappings
type OpensearchRoleMapping struct {
	Users        []string `json:"users"`
	BackendRoles []string `json:"backend_roles"`
	Hosts        []string `json:"hosts"`
}

// Returns a "302 Found" response
// If the parameter "redirectUrl" is present in the request it is used,
// otherwise fall back to the bare OpenSearch host
func redirectToOpenSearch(w http.ResponseWriter, r *http.Request) {
	redirectUrl := r.URL.Query().Get("redirectUrl")
	if redirectUrl == "" {
		u, _ := url.Parse(opensearchApiUrl)
		redirectUrl = (&url.URL{
			Scheme: u.Scheme,
			Host:   u.Host,
			// set the correct tenant by default
			RawQuery: fmt.Sprintf("security_tenant=%s", opensearchTenantName),
		}).String()
	}
	log.Printf("Redirecting user to '%s'", redirectUrl)
	http.Redirect(w, r, redirectUrl, http.StatusFound)
}

// Returns the list of projects a user (identified by the access token) has access to.
func getProjectsForUser(token string) ([]string, error) {
	var projectNames []string
	request, err := http.NewRequest("GET", clusterApiUrl+"/apis/project.openshift.io/v1/projects?labelSelector="+url.QueryEscape(projectLabelSelector), nil)
	request.Header.Set("authorization", "Bearer "+token)
	resp, err := httpClient.Do(request)
	if err != nil {
		return projectNames, fmt.Errorf("Failed to fetch list of projects: %q", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return projectNames, fmt.Errorf("Got unexpected response from Kubernetes API server: %s", resp.Status)
	}

	var parsedResp ProjectResponse
	err = json.NewDecoder(resp.Body).Decode(&parsedResp)
	// err = json.NewDecoder(io.TeeReader(resp.Body, os.Stdout)).Decode(&parsedResp)
	if err != nil {
		return projectNames, fmt.Errorf("Failed to parse project JSON response: %q", err)
	}

	for _, project := range parsedResp.Items {
		projectNames = append(projectNames, project.Metadata.Name)
	}

	return projectNames, nil
}

// Creates one role per cluster+username combination
func syncOpensearchPermissionsForUser(username string, projects []string, clusterName string) error {
	// keeping this as a raw string (instead of Go struct) because it's much easier to handle and read due to the deep nesting
	projectList, _ := json.Marshal(projects) // convert to json-format, i.e. `["foo","bar","baz"]`
	dls := fmt.Sprintf(`
{
  "bool": {
    "must": [
      {
        "term": {
          "data.cluster_name": "%s"
        }
      },
      {
        "terms": {
          "data.namespace": %s
        }
      }
    ]
  }
}
`,
		clusterName, projectList)

	// Create role
	role, err := json.Marshal(OpensearchRole{
		Description:        getRoleDescription(),
		ClusterPermissions: []string{},
		IndexPermissions: []IndexPermissions{
			{
				IndexPatterns:  []string{opensearchIndexPattern},
				DLS:            dls,
				AllowedActions: []string{"read"},
			},
		},
		TenantPermissions: []TenantPermissions{
			{
				TenantPatterns: []string{opensearchTenantName},
				AllowedActions: []string{"kibana_all_read"}, // read-only access to the tenant
			},
		},
	})
	if err != nil {
		return fmt.Errorf("Failed to marshal role: %q", err)
	}

	roleName := fmt.Sprintf("okd-user_%s_%s", clusterName, username)
	debugLogf("DEBUG ROLE %s: %s", roleName, role)

	_, err = opensearchApi("PUT", "/_plugins/_security/api/roles/"+roleName, bytes.NewReader(role))
	if err != nil {
		return fmt.Errorf("Failed to create Opensearch role %s: %q", roleName, err)
	}

	// also create the necessary rolemapping
	roleMapping, err := json.Marshal(OpensearchRoleMapping{
		Users:        []string{username},
		BackendRoles: []string{},
		Hosts:        []string{},
	})
	if err != nil {
		return fmt.Errorf("Failed to marshal rolemapping: %q", err)
	}
	debugLogf("DEBUG ROLEMAPPING: %s", roleMapping)
	_, err = opensearchApi("PUT", "/_plugins/_security/api/rolesmapping/"+roleName, bytes.NewReader(roleMapping))
	if err != nil {
		return fmt.Errorf("Failed to create Opensearch rolemapping %s: %q", roleName, err)
	}

	return nil // all good
}

func synchronizeOpensearchPermissions(r *http.Request) error {
	// get username from header set by the oauth proxy
	username := r.Header.Get("x-forwarded-user")
	if username == "" {
		return fmt.Errorf("No username found in request headers")
	}

	token := r.Header.Get("x-forwarded-access-token")
	if token == "" {
		return fmt.Errorf("No Kubernetes access token found in request headers")
	}

	// get list of projects the user has access to (== admin access)
	projects, err := getProjectsForUser(token)
	if err != nil {
		return err
	}

	// This is a safeguard to prevent synchronizing the permissions of cluster-admin accounts
	// which have access to hundreds of projects.
	// Complex DLS rules (with hundreds of conditions) significantly slow down OpenSearch.
	if len(projects) > 120 {
		return fmt.Errorf("Refusing to synchronize permissions because account '%s' has access to %d projects (likely an admin account)", username, len(projects))
	}

	log.Printf("Synchronizing permissions for %s: %q (%s)", username, projects, clusterName)
	err = syncOpensearchPermissionsForUser(username, projects, clusterName)
	if err != nil {
		return err
	}

	return nil // all good
}

// Simple wrapper around the OpenSearch API
func opensearchApi(method string, uri string, body io.Reader) (map[string]interface{}, error) {
	var data map[string]interface{}

	debugLogf("DEBUG Calling OpenSearch API %s %s", method, uri)
	request, err := http.NewRequest(method, opensearchApiUrl+uri, body)
	request.Header.Set("content-type", "application/json")
	request.SetBasicAuth(opensearchApiUsername, opensearchApiPassword)
	if err != nil {
		return data, err
	}
	response, err := httpClient.Do(request)
	if err != nil {
		return data, err
	}
	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		return data, err
	}

	if response.StatusCode >= 300 {
		return data, fmt.Errorf("ERROR: OpenSearch API returned %s for %s %s: %q", response.Status, method, uri, data)
	}

	debugLogf("DEBUG OpenSearch API response %s %v", response.Status, data)

	return data, nil
}

func getRoleDescription() string {
	// Storing some metadata in the description so we can later identify roles which have not been updated for a while
	description, err := json.Marshal(OpensearchRoleDescription{
		CreatedBy:   "opensearch-user-permissions",
		LastUpdated: time.Now().Format(time.RFC3339),
	})
	if err != nil {
		panic("Failed to JSON-marshal role description: " + err.Error())
	}

	return string(description)
}

// Creates a basic role that allows access to the security tenant, but does not grant any permissions to view logs) and assigns it to all users.
// This is necessary so that users that haven't yet synchronized their permissions will see the Welcome dashboards and be informed
// that they need to visit the synchronization endpoint (this component).
func createBasePermissions() error {
	// Create role
	role, err := json.Marshal(OpensearchRole{
		Description:        getRoleDescription(),
		ClusterPermissions: []string{},
		IndexPermissions:   []IndexPermissions{}, // no permissions on any index
		TenantPermissions: []TenantPermissions{
			{
				TenantPatterns: []string{opensearchTenantName},
				AllowedActions: []string{"kibana_all_read"}, // read-only access to the tenant
			},
		},
	})
	if err != nil {
		return fmt.Errorf("Failed to marshal role: %q", err)
	}

	roleName := opensearchTenantName // use the same name as for the security tenant
	_, err = opensearchApi("PUT", "/_plugins/_security/api/roles/"+roleName, bytes.NewReader(role))
	if err != nil {
		return fmt.Errorf("Failed to create Opensearch role %s: %q", roleName, err)
	}

	roleMapping, err := json.Marshal(OpensearchRoleMapping{
		Users:        []string{"*"}, // applies to all users
		BackendRoles: []string{},
		Hosts:        []string{},
	})
	if err != nil {
		return fmt.Errorf("Failed to marshal rolemapping: %q", err)
	}
	debugLogf("DEBUG ROLEMAPPING: %s", roleMapping)
	_, err = opensearchApi("PUT", "/_plugins/_security/api/rolesmapping/"+roleName, bytes.NewReader(roleMapping))
	if err != nil {
		return fmt.Errorf("Failed to create Opensearch rolemapping %s: %q", roleName, err)
	}

	return nil // all good
}

func main() {
	// shared HTTP client
	httpClient = &http.Client{}

	// use Kubernetes CA if available (required for connecting to Kubernetes API)
	var caCertFile = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
	if _, err := os.Stat(caCertFile); !os.IsNotExist(err) {
		// load CA certificate from file
		caCert, err := os.ReadFile(caCertFile)
		if err != nil {
			log.Fatal(err)
		}
		// get a copy of the system-wide certificate pool
		caCertPool, err := x509.SystemCertPool()
		if err != nil {
			log.Fatal(err)
		}
		// append the k8s CA to the pool
		caCertPool.AppendCertsFromPEM(caCert)
		// use the new pool for HTTP connections
		httpClient.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			},
		}
	}

	// set up base permissions
	err := createBasePermissions()
	if err != nil {
		log.Fatalf("Failed to set up base permissions: %s", err)
	}

	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "pong")
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		err := synchronizeOpensearchPermissions(r)
		if err != nil {
			log.Printf("ERROR: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "An error occured. Please request help from the administrators: https://paas.docs.cern.ch/contact/")
			return
		}
		redirectToOpenSearch(w, r)
	})
	log.Printf("Starting HTTP server on %s ...", httpListenAddress)
	log.Fatal(http.ListenAndServe(httpListenAddress, nil))
}
