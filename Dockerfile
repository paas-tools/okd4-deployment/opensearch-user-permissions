FROM registry.cern.ch/docker.io/library/golang:1.21 AS builder
WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 go build -o app

# Use CERN's ALMA 9 base image because it includes CERN / GRID CA root (used by OpenSearch endpoint)
FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
COPY --from=builder /build/app /usr/bin/
ENTRYPOINT ["/usr/bin/app"]
